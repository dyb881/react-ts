# 已重构到 [react-app](https://github.com/dyb881/react-app)

# react 项目包

---

[<img src='https://gitee.com/dyb881/react-ts/badge/star.svg?theme=dark' alt='star'></img>](https://gitee.com/dyb881/react-ts/stargazers)<br>
基于 create-react-app v2 --typescript 的项目工程<br>
[使用 react-app-rewired 拓展配置](https://gitee.com/dyb881/react-ts/blob/master/config-overrides.js)<br>
[状态管理默认使用 mobx](https://gitee.com/dyb881/react-ts/blob/master/src/store/index.ts)<br>
[并已用 react-router 搭建页面框架，在页面配置文件配置即可添加新页面](https://gitee.com/dyb881/react-ts/blob/master/src/page/config.ts)<br>
默认使用 antd | antd-mobile 作为主体 UI,已配置按需加载，推荐在 component 内部二次封装再导出，方便统一修改<br>
支持 electron 跨平台桌面应用开发，需要自行引用包<br>
这并不是一个框架，而是一个 react 项目的文件管理习惯<br>
每一次开发项目都可进行优化以及一些组件和工具函数的加入<br>
定期更新依赖版本<br>

### 开发模式

---

业务上拆分为一级路由，一个页面一个文件夹，注册在 page/config.ts<br>
数据储存也按业务区分为单个文件到 store 并实例化到 store/index.ts 中，组成一个主 store，多个子 store，在 App.tsx 中注入，注入前可执行预设，比如自动登录判断等操作<br>
外部引用大组件适当使用import()分包<br>
如果是React组件 可以使用 React.lazy() and <React.Suspense><br>
[TsLint代码审查](https://gitee.com/dyb881/react-ts/blob/master/tslint.json)
[项目文档](https://gitee.com/dyb881/react-ts/blob/master/PROJECT.md)

### 技术栈

---

- 使用语言
  - ES next
  - TypeScript
- 脚手架
  - create-react-app 主体
  - react-app-rewired 拓展配置
  - tslint 代码审查
- CSS 处理
  - 预处理 less scss sass
  - 后处理 postcss
  - 初始化 normalize.css
  - 模块化 CSS Modules
- 默认依赖
  - 热更新 react-hot-loader
  - 动画 react-addons-css-transition-group
  - 路由 react-router
  - UI库 antd antd-mobile
  - 状态管理 mobx mobx-react
  - 接口拦截并返回模拟数据数据 mockjs fetch-mock
  - 图片预览 react-zmage [已使用动态加载 /src/component/file/img/index.tsx 第7行](https://gitee.com/dyb881/react-ts/blob/master/src/component/file/img/index.tsx)
- 工具函数
  - [fetch 异步请求封装](https://gitee.com/dyb881/react-ts/blob/master/src/tool/request/index.ts)
  - [dom 插入工具](https://gitee.com/dyb881/react-ts/blob/master/src/tool/dom.ts)
  - [文件转化以及图片裁剪](https://gitee.com/dyb881/react-ts/blob/master/src/tool/file.ts)

### 目录

---

```
│  config-overrides.js react-app-rewired 配置文件
│  NORM.md 开发规范文档
│  PROJECT.md 项目文档
│  README.md 描述文档
│  tsconfig.json ts编译配置
│  tslint.json ts代码审查配置
├─public
│  config.js 项目配置
└─src
  ├── App.less 全局样式
  ├── App.test.tsx
  ├── App.tsx 项目初始化
  ├── api.ts API请求工具初始化
  ├── api_mock.ts API模拟数据注册
  ├── react-app-env.d.ts 外部引用类型描述
  ├── config.ts 主配置
  ├── index.tsx 入口文件
  ├── component 组件
  │   ├── mobile 移动端组件
  │   │   ├── antd antd 组件二次封装
  │   │   ├── header 标题栏
  │   │   └── tabBar 底部导航
  │   ├── pc pc端组件
  │   │   └── antd antd 组件二次封装
  │   ├── file 文件
  │   │   ├── get_file.tsx 获取文件
  │   │   └── img 图片组件 - 包含预览功能
  │   ├── form 表单工具组件
  │   ├── router 批量路由页面注册
  │   └── transition/style.less 全局 react-addons-css-transition-group 动画注册
  ├── interface 公共接口
  ├─page 所有页面
  │  │  config.ts 页面配置
  │  │  index.tsx 注册及主体页面
  │  ├─Folder1 首页1
  │  └─Folder2 页面2
  ├─store 状态管理
  │  index.ts 主状态
  │  view.ts  视图状态
  │  user.ts  用户状态
  │  File1.ts 子状态1
  │  File2.ts 子状态2
  └── tool 工具函数
      ├── city.json 省市县数据
      ├── dom.ts dom相关
      ├── file.ts 文件相关
      ├── function.less less函数
      ├── index.ts 常用
      ├── mock.ts 模拟数据
      ├── reg_exp.ts 正则
      ├── request 请求封装
      └── string. 字符串相关
```

### 安装教程

---

1. npm config set registry https://registry.npm.taobao.org // 指定到淘宝镜像
2. npm install --global yarn // 安装 yarn
4. yarn // 安装项目
5. yarn start // 开发模式
6. yarn build // 打包
7. yarn serve // 以服务运行打包后的文件
8. yarn electron-start // 运行客户端开发模式，前提需要先运行 yarn start
9. yarn packager // 客户端打包，前提需要先运行 yarn build
