import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { IPage } from '../../../interface';
import { Icon } from 'antd-mobile';
import style from './style.module.less';

interface IProps extends IPage {
  showBoBack?: (pathname: string) => boolean;
}

@inject('store')
@observer
class Header extends React.Component<IProps> {
  showBoBack = (pathname: string) => pathname !== '/';

  render() {
    const { store, showBoBack = this.showBoBack, location, history } = this.props;
    return (
      <header className={style.header}>
        <div className={style.left}>{showBoBack(location.pathname) && <Icon type="left" size="lg" onClick={history.goBack} />}</div>
        {<div className={style.center}>{store!.view.title}</div>}
        <div className={style.right} />
      </header>
    );
  }
}

export default withRouter(Header);
