import React from 'react';
import { Spin as Spin_, Modal as Modal_ } from 'antd';
import { SpinProps } from 'antd/es/spin';
import { ModalFuncProps } from 'antd/es/modal';
import style from './style.module.less';

/**
 * 加载图标
 * @param  {[type]}    options.className [description]
 * @param  {...[type]} options.props     [description]
 * @return {[type]}                      [description]
 */
export const Spin: React.SFC<SpinProps> = ({ className, ...props }) => (
  <Spin_ className={[style.spin, className].join(' ')} {...props} />
);

/**
 * 弹窗函数
 */
export const {
  confirm, // 对话确认框
} = Modal_;

/**
 * 对话确认框
 */
export class ModalConfirm extends React.Component<ModalFuncProps> {
  onClick = () => {
    const { children, ...props } = this.props;
    confirm(props);
  };

  render() {
    const { children } = this.props;
    return <span onClick={this.onClick}>{children}</span>;
  }
}
