import React from 'react';
import { Input as Input_, InputNumber as InputNumber_ } from 'antd';
import { InputProps, TextAreaProps } from 'antd/es/input';
import { InputNumberProps } from 'antd/es/input-number';
import style from './style.module.less';

/**
 * 文本框
 */
export class Input extends React.Component<InputProps> {
  render() {
    return <Input_ maxLength={30} autoComplete="off" {...this.props} />;
  }
}

/**
 * 大文本框
 */
export class TextArea extends React.Component<TextAreaProps> {
  render() {
    const { value, maxLength = 255, ...props } = this.props;
    return (
      <div className={style.textArea}>
        <Input_.TextArea rows={4} maxLength={maxLength} value={value} {...props} />
        <p>
          {('' + value).length} / {maxLength}
        </p>
      </div>
    );
  }
}

/**
 * 密码输入框
 */
export class Password extends React.Component<InputProps> {
  render() {
    return <Input_.Password maxLength={30} {...this.props} />;
  }
}

/**
 * 数字文本框
 */
export class InputNumber extends React.Component<InputNumberProps> {
  render() {
    return <InputNumber_ max={10000000} step={1} precision={0} {...this.props} />;
  }
}
