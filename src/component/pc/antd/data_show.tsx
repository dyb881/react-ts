import React from 'react';
import { Table as Table_, Pagination as Pagination_ } from 'antd';
import { TableProps } from 'antd/es/table';
import { PaginationProps } from 'antd/es/pagination';
import style from './style.module.less';

/**
 * 表格
 * @param  {[type]} {  pagination,  ...props}) [description]
 * @return {[type]}                          [description]
 */
export const Table: React.SFC<TableProps<any>> = ({ pagination, ...props }) => (
  <Table_
    rowKey="id"
    style={{ width: '100%' }}
    pagination={{
      showTotal: (total: number) => `总计 ${total} 条`,
      ...pagination,
    }}
    {...props}
  />
);

/**
 * 分页
 * @param  {[type]} props [description]
 * @return {[type]}       [description]
 */
export const Pagination: React.SFC<PaginationProps> = props => (
  <div className={style.pagination}>
    <Pagination_ showTotal={(total: number) => `总计 ${total} 条`} {...props} />
  </div>
);

/**
 * 单条信息
 * @type {[type]}
 */
export const InfoItem: React.SFC<{
  label: string;
  fill?: boolean;
}> = ({ label, fill, children }) => (
  <div className={[style.infoItem, fill ? style.fill : undefined].join(' ')}>
    <span className={style.label}>{label}</span>
    <div className={style.main}>{children}</div>
  </div>
);
