import React from 'react';
import { Select as Select_, Radio as Radio_, TreeSelect as TreeSelect_, Cascader as Cascader_ } from 'antd';
import { SelectProps } from 'antd/es/select';
import { RadioProps } from 'antd/es/radio';
import { TreeSelectProps } from 'antd/es/tree-select';
import { CascaderProps } from 'antd/es/cascader';
import { GetBox } from './not_classified';

type IOptionValue = string | number;

interface IOption {
  label: string | JSX.Element; // 选择的内容
  value: IOptionValue; // 选择的值
  disabled?: boolean;
  [key: string]: any;
}

interface IOptions {
  options: IOption[] | IOptionValue[];
  disableds?: IOptionValue[];
}

/**
 * 转为选项值
 * @type {Function}
 */
const toOptions = (options: IOptions['options']): IOption[] => {
  const type = typeof options[0];
  if (type === 'string' || type === 'number') {
    return (options as IOptionValue[]).map((i, k) => ({
      label: '' + i,
      value: k,
    }));
  }
  return options as IOption[];
};

/**
 * 禁用值合并
 * @param  {[type]} options:  IOption[]     [description]
 * @param  {[type]} disableds ?             :             IOptionValue[] [description]
 * @return {[type]}           [description]
 */
const toDisableds = (options: IOption[], disableds?: IOptionValue[]) => {
  if (disableds) {
    options = options.map(i => {
      if (disableds.indexOf(i.value) > -1) {
        i.disabled = true;
      }
      return i;
    });
  }
  return options;
};

/**
 * 筛选
 * @param  {[type]} input:  string        [description]
 * @param  {[type]} option: any           [description]
 * @return {[type]}         [description]
 */
const selectFilterOption = (input: string, option: any) =>
  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

/**
 * 选择器
 */
export class Select extends React.Component<SelectProps & IOptions> {
  render() {
    const { options, disableds, ...props } = this.props;
    let options_ = toOptions(options);
    options_ = toDisableds(options_, disableds);
    return (
      <GetBox>
        {getBox => (
          <Select_
            style={{ width: 500 }}
            optionLabelProp="title"
            getPopupContainer={getBox}
            filterOption={selectFilterOption}
            placeholder="请选择"
            {...props}
          >
            {options_.map(({ label, title = label, ...i }) => (
              <Select_.Option key={'' + i.value} title={title} {...i}>
                {label}
              </Select_.Option>
            ))}
          </Select_>
        )}
      </GetBox>
    );
  }
}

interface IRadioProps extends RadioProps, IOptions {
  isButton?: boolean; // 启用按钮样式
}

/**
 * 单选框
 */
export class Radio extends React.Component<IRadioProps> {
  render() {
    const { options, disableds, isButton, ...props } = this.props;
    let options_ = toOptions(options);
    options_ = toDisableds(options_, disableds);
    let RadioItem: any;
    if (isButton) {
      RadioItem = Radio_.Button;
    } else {
      RadioItem = Radio_;
    }
    return (
      <Radio_.Group buttonStyle="solid" {...props}>
        {options_.map(({ label, ...i }) => (
          <RadioItem key={'' + i.value} {...i}>
            {label}
          </RadioItem>
        ))}
      </Radio_.Group>
    );
  }
}

/**
 * 树选择
 */
export class TreeSelect extends React.Component<TreeSelectProps> {
  render() {
    return <GetBox>{getBox => <TreeSelect_ getPopupContainer={getBox} {...this.props} />}</GetBox>;
  }
}

/**
 * 级联选择
 */
export class Cascader extends React.Component<CascaderProps> {
  render() {
    return <GetBox>{getBox => <Cascader_ getPopupContainer={getBox} {...this.props} />}</GetBox>;
  }
}
