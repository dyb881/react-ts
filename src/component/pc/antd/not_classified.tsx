import React from 'react';
import { Button as Button_ } from 'antd';
import { ButtonProps } from 'antd/es/button';

interface IGetBoxProps {
  children: (getBox: () => HTMLSpanElement) => JSX.Element;
}

/**
 * 获取父元素
 */
export class GetBox extends React.Component<IGetBoxProps> {
  box: HTMLSpanElement | null = null;

  render() {
    const children = this.props.children;
    return <span ref={box => (this.box = box)}>{children && children(() => this.box!)}</span>;
  }
}

/**
 * 按钮
 * @param  {[type]} props [description]
 * @return {[type]}       [description]
 */
export const Button: React.SFC<ButtonProps> = props => <Button_ type="primary" {...props} />;

