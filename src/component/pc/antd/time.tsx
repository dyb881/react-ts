import React from 'react';
import { DatePicker as DatePicker_ } from 'antd';
import { DatePickerProps, WeekPickerProps, MonthPickerProps, RangePickerProps } from 'antd/es/date-picker/interface';
import { GetBox } from './not_classified';

/**
 * 时间选择器
 */
export class DatePicker extends React.Component<DatePickerProps> {
  render() {
    return <GetBox>{getBox => <DatePicker_ getCalendarContainer={getBox} {...this.props} />}</GetBox>;
  }
}

/**
 * 周
 */
export class WeekPicker extends React.Component<WeekPickerProps> {
  render() {
    return <GetBox>{getBox => <DatePicker_.WeekPicker getCalendarContainer={getBox} {...this.props} />}</GetBox>;
  }
}

/**
 * 月
 */
export class MonthPicker extends React.Component<MonthPickerProps> {
  render() {
    return <GetBox>{getBox => <DatePicker_.MonthPicker getCalendarContainer={getBox} {...this.props} />}</GetBox>;
  }
}

/**
 * 范围
 */
export class RangePicker extends React.Component<RangePickerProps> {
  render() {
    return <GetBox>{getBox => <DatePicker_.RangePicker getCalendarContainer={getBox} {...this.props} />}</GetBox>;
  }
}
