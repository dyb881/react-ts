import React from 'react';
import { LocaleProvider as LocaleProvider_ } from 'antd';
import zh_CN from 'antd/es/locale-provider/zh_CN';
import moment from 'moment';
import 'moment/locale/zh-cn';

moment.locale('zh-cn');

/**
 * 设置国际化支持
 * @param  {[type]} options.children [description]
 * @return {[type]}                  [description]
 */
export const LocaleProvider: React.SFC = ({ children }) => <LocaleProvider_ locale={zh_CN}>{children}</LocaleProvider_>;

export * from './not_classified'; // 未分类
