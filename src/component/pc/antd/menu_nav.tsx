import React from 'react';
import { Menu } from 'antd';
import { ClickParam } from 'antd/es/menu';
import { withRouter } from 'react-router-dom';
import { Icon } from 'antd';
import { IRoute } from '../../../interface';
import style from './style.module.less';

const { SubMenu, Item } = Menu;

interface IChild {
  to: string;
  title: string;
  icon?: string;
  onClick?: () => void;
}

interface IData extends IChild {
  child?: IChild[];
}

interface IProps extends IRoute {
  data: IData[];
  reload?: () => void;
}

interface IState {
  openKeys: string[];
  selectedKeys: string[];
}

class MenuNav_ extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const [openKey, selectedKey] = this.getKeys(props.location.pathname);
    this.state = {
      openKeys: openKey ? [openKey] : [],
      selectedKeys: [selectedKey],
    };
  }

  getKeys = (pathname: string) => {
    const { data } = this.props;
    const test = (to: string) => new RegExp(`^${to}`).test(pathname);
    let openKey: string;
    let selectedKey: string;
    data.forEach((i, k) => {
      if (i.child) {
        i.child.forEach((is, ks) => {
          if (test(is.to)) {
            openKey = `sub-menu-${k}`;
            selectedKey = `item-${k}-${ks}`;
          }
        });
      } else if (test(i.to)) {
        selectedKey = `item-${k}`;
      }
    });
    return [openKey!, selectedKey!];
  };

  componentWillReceiveProps(nextProps: IProps) {
    const { pathname } = this.props.location;
    const { pathname: nextPathname } = nextProps.location;
    const { openKeys } = this.state;
    if (!new RegExp(`^${nextPathname}`).test(pathname)) {
      const [openKey, selectedKey] = this.getKeys(nextPathname);
      openKey && openKeys.indexOf(openKey) === -1 && openKeys.push(openKey);
      this.setState({
        openKeys,
        selectedKeys: [selectedKey],
      });
    }
  }

  onOpenChange = (openKeys: string[]) => {
    this.setState({
      openKeys,
    });
  };

  onClick = ({ key }: ClickParam) => {
    const { data, history, location, reload } = this.props;
    const keys = key.split('-');
    let item = data[keys[1]];
    if (item.child) {
      item = item.child[keys[2]];
    }
    item.onClick && item.onClick();
    if (location.pathname === item.to) {
      reload ? reload() : window.location.reload();
    } else {
      history.push(item.to);
    }
  };

  render() {
    const { data } = this.props;
    const { openKeys, selectedKeys } = this.state;
    return (
      <Menu
        mode="inline"
        openKeys={openKeys}
        selectedKeys={selectedKeys}
        onOpenChange={this.onOpenChange}
        onClick={this.onClick}
        className={style.menuNav}
      >
        {data.map((i: any, k: number) =>
          i.child ? (
            <SubMenu key={`sub-menu-${k}`} title={<Title data={i} />}>
              {i.child.map((is: any, ks: number) => (
                <Item key={`item-${k}-${ks}`}>
                  <Title data={is} />
                </Item>
              ))}
            </SubMenu>
          ) : (
            <Item key={`item-${k}`}>
              <Title data={i} />
            </Item>
          )
        )}
      </Menu>
    );
  }
}

const Title: React.SFC<{
  data: any;
}> = ({ data }) => (
  <>
    {data.icon && <Icon type={data.icon} />}
    <span>{data.title}</span>
  </>
);

export const MenuNav = withRouter(MenuNav_);

export default MenuNav;
