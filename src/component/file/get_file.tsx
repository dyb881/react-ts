import React from 'react';
import { IChangeEvent } from '../../interface';

interface IProps {
  className?: string;
  accept?: string;
  multiple?: boolean;
  onChange: (files: FileList) => void;
}

export default class GetFile extends React.Component<IProps> {
  onChange = (e: IChangeEvent): void => {
    const files = e.target.files;
    files && this.props.onChange(files);
  };

  render() {
    const { className, accept = 'image/*', multiple = false, children } = this.props;
    return (
      <label className={className}>
        <input type="file" accept={accept} multiple={multiple} onChange={this.onChange} value={''} hidden={true} />
        {children}
      </label>
    );
  }
}
