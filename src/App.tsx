import React from 'react';
import { hot } from 'react-hot-loader';
import { HashRouter as Router } from 'react-router-dom';
import { Provider } from 'mobx-react';
import Store from './store';
import Page from './page';
import 'normalize.css';
import './App.less';
// import './api_mock'; // 模拟数据

// 初始化并缓存全局状态，热更新友好
window.store = window.store || new Store();
const store = window.store as Store;

// 状态初始化设置
store.view.setTitle('项目');
// 尝试新项目 https://gitee.com/dyb881/react-limit
// 更完善文档，更优美的文件管理方案，更加简洁明了的开发模式

const App = () => (
  <Provider store={store}>
    <Router>
      <Page />
    </Router>
  </Provider>
);

// 输出热更新
export default hot(module)(App);
