import Mock, { Random } from './tool/mock';
import { baseURL } from './api';
import { phone } from './tool/reg_exp';

const mock = Mock(baseURL);

const user = {
  id: '12345678910',
  userName: Random.word(8),
  realName: Random.cname(),
  mobilePhone: phone,
  email: Random.email(),
  depart: Random.ctitle()
};

/**
 * 模拟数据接口定义
 */
mock
  .post('/user/send')
  .post('/user/login', user)
  .get('/user/userInfo', user);
