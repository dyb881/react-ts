import qs from 'qs';
import Request from './tool/request';
import { requestConfig } from './config';

// 控制台打印颜色
const consoleStyle = {
  request: 'color: #0089E5;',
  success: 'color: #2DB700;',
  fail: 'color: #F41900;',
};

// 数据类型
const application = {
  json: 'application/json',
  form: 'application/x-www-form-urlencoded',
};

// 请求模块初始化并输出请求方法以及参数
export const { baseURL, photo, request, get, post, put, del, upload } = Request({
  ...requestConfig, // 请求配置
  defaultConfig: {
    // 默认配置
    method: 'get', // 请求类型
    mode: 'cors',
    dataFormat: 'json', // 返回数据解析格式
    headers: {
      Accept: application.json, // 期望得到数据格式
      'Content-type': application.json, // 传递参数格式
    },
  },
  // 请求拦截
  interceptorsRequest: (url, config) => {
    if (['post', 'put', 'delete'].indexOf(config.method) > -1) {
      const type = config.headers['Content-type'];
      let body = ''; // 根据传递参数格式转换数据
      if (type === application.json) {
        body = JSON.stringify(config.data);
      } else if (type === application.form) {
        body = qs.stringify(config.data);
      }
      if (['', '{}', '[]'].indexOf(body) === -1) {
        config.body = body; // 不为空时把数据写入请求主体
      }
    }
    console.groupCollapsed(`%cRequest: ${config.label || config.address} ⇅`, consoleStyle.request);
    console.log('请求类型：', config.method.toUpperCase());
    console.log('请求地址：', url);
    console.log('请求数据：', config.data || '无');
    console.log('请求配置：', config);
    console.groupEnd();
    return config;
  },
  // 响应拦截
  interceptorsResponse: (url, config, res) => {
    if (!res.errorText && +res.code !== 2000 && +res.code !== 0) {
      res.errorText = res.msg || '请求异常';
    }
    const success = !res.errorText; // 请求结果状态
    console.groupCollapsed(
      `%cResponse: ${config.label || config.address} ${success ? '√' : '×'}`,
      consoleStyle[success ? 'success' : 'fail']
    );
    console.log('响应数据：', res);
    if (success) {
      console.log('返回数据：', res.data);
    } else {
      console.log('响应异常：', res.error || res.status || '无');
      console.log('异常解析：', res.errorText);
    }
    console.groupEnd();
    return res;
  },
});

