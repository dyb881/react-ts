// 错误解析字段
const erroText = {
  timeout: '网络连接超时',
  'Network Error': '请求地址错误或跨域未允许',
  'Failed to fetch': '请求地址错误或跨域未允许',
  'JSON at position': '返回数据非json格式导致解析错误',
  'No JSON': '返回数据非json格式导致解析错误',
};

/**
 * 异常分析
 * @param  {string} error 异常码
 * @return {string}       异常解析文本
 */
export const erroToText = (error: string): string => {
  for (const reg in erroText) {
    if (new RegExp(reg).test(error)) {
      return erroText[reg];
    }
  }
  return '其他错误';
};

/**
 * 处理 config 内部的 label
 * @param  {any} config 配置信息
 * @return {any}        配置信息
 */
export const toLabel = (config: any) => {
  if (typeof config === 'string') {
    return {
      label: config,
    };
  }
  return config;
};

// 值类型
export const valueType = ['string', 'number', 'boolean'];

/**
 * 对象数据写入表单对象
 * @param  {any}      data 原始数据
 * @return {FormData}      表单对象
 */
export const getFormData = (data?: any) => {
  const body = new FormData();
  if (!data) return body;
  const setData = (data: any, key = '') => {
    Object.keys(data).forEach(i => {
      const item = data[i];
      if (valueType.indexOf(typeof item) > -1) {
        body.append(key ? `${key}[${i}]` : i, item);
      } else if (item) {
        setData(item, key ? `${key}[${i}]` : i);
      }
    });
  };
  setData(data);
  return body;
};
