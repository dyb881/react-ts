/**
 * rem 自动计算
 * 根据浏览器宽度对比最大宽度，得出百分值，并转化为像素写入 html.style.font-size
 * @param  {number} max = 750 最大宽度
 * @param  {number} min = 320 最小宽度
 * @return {() => void}       返回计算函数，默认调用一次
 */
export const autosize = (max = 750, min = 320) => {
  const htmlstyle: any = document.getElementsByTagName('html')[0].style;
  const w: number = innerWidth;
  const resize: () => void = () => {
    htmlstyle.fontSize = (((w > max && max) || (w < min && min) || w) / max) * 100 + 'px';
  };
  resize();
  return resize;
};

/**
 * 异步暂停
 * @param  {number} timeOut 暂停时间
 */
export const sleep = (timeOut: number) => new Promise(resolve => setTimeout(resolve, timeOut));

/**
 * 获取范围内随机数
 * @param  {number} min 最小值
 * @param  {number} max 最大值
 */
export const randomInt = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;
