import React from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import { IPage } from '../../interface';
import style from './style.module.less';

@inject('store')
@observer
export default class Info extends React.Component<IPage> {
  render() {
    return (
      <div>
        <h1 className={style.title}>首页</h1>
        <Link to="/test">跳转到测试</Link>
        <Test test />
      </div>
    );
  }
}

const Test: React.SFC<{
  test: boolean;
}> = ({ test }) => {
  const [val, setVal] = React.useState(0);
  React.useEffect(() => {
    console.log(val);
  });
  return <Button onClick={() => setVal(val + 1)} />;
};
