import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { IPage } from '../../interface';

@inject('store')
@observer
export default class Home extends React.Component<IPage> {
  render() {
    return (
      <div className={`fill`}>
        <h1>测试</h1>
        <Link to="/home">跳转到首页</Link>
      </div>
    );
  }
}
