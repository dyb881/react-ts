import Home from './home';
import Test from './test';

/**
 * 导出页面
 */
export const page = {
  '/home': Home,
  '/test': Test,
};
