import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { IPage } from '../interface';
import { page } from './config';
import Router from '../component/router';

@inject('store')
@observer
class Page extends React.Component<IPage> {
  render() {
    return (
      <div className="fill">
        <Router page={page} transition={true} />
      </div>
    );
  }
}

export default withRouter(Page);
