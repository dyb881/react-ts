/**
 * 全局配置处理
 */
const {
  location: { hostname, search, port },
  config: { request },
} = window;

// 链接参数
export let params: any = {};
if (search) {
  params = search
    .slice(1)
    .split('&')
    .map(v => v.split('='))
    .reduce((o: any, [k, v]) => ((o[k] = v), o), {});
}

// 请求配置
export const requestConfig: any = {
  host: '', // API地址
  apiPath: '', // API入口路径
  photoPath: '', // 图片文件路径
  timeout: 5000, // 请求超时限制
};

// 配置文件重写
Object.keys(request).forEach(i => {
  if (request[i]) requestConfig[i] = request[i];
});

const hosts = {
  production: request.host, // 生产环境
  test: '', // 测试环境
};

const isText = hostname === ''; // 测试环境
const isLocal = port === '3000' || /^file:/.test(hostname); // 本地环境

if (isText || isLocal) {
  requestConfig.host = hosts[params.type] || hosts.test;
}

// 上传文件最大尺寸
export const uploadFileMaxSize = 1024 * 1024 * 10;
