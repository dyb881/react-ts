import { configure, observable, action } from 'mobx';
import View from './view';
import User from './user';

configure({
  enforceActions: 'observed',
});

// 主状态
export default class Store {
  view = new View(this); // 视图相关管理
  user = new User(this); // 用户管理

  @observable networkConnection: boolean = navigator.onLine; // 网络连接状态

  constructor() {
    // 监听网络链接状态
    window.ononline = action(() => (this.networkConnection = true));
    window.onoffline = action(() => (this.networkConnection = false));
  }
}
