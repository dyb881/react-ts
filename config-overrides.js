const { override, addBabelPlugin, fixBabelImports, addLessLoader } = require('customize-cra');
const Merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const env = process.env.NODE_ENV;

const addImportAnt = libraryName =>
	fixBabelImports(libraryName, {
		libraryName,
		libraryDirectory: 'es',
		style: true,
	});

module.exports = override(
	addBabelPlugin('react-hot-loader/babel'),
	addImportAnt('antd'),
	addImportAnt('antd-mobile'),
	addLessLoader({
		javascriptEnabled: true,
		modifyVars: {},
	}),
	// 自定义更改
	config => {
		// 添加ts代码审查
		const forkTsCheckerWebpackPlugin = config.plugins[config.plugins.length - 1];
		forkTsCheckerWebpackPlugin.tslint = './tslint.json';
		forkTsCheckerWebpackPlugin.tslintVersion = require('tslint').Linter.VERSION;

		if (env === 'production') {
			config = Merge(config, {
				devtool: false,
				optimization: {
					// 压缩代码
					minimizer: [
						new UglifyJsPlugin({
							cache: true, // 启用文件缓存
							parallel: true, // 使用多线程
							uglifyOptions: {
								compress: {
									warnings: false, // 删除无用代码时不输出警告
									drop_console: true, // 删除console语句
									collapse_vars: true, // 内嵌定义了但是只有用到一次的变量
									reduce_vars: true, // 提取出出现多次但是没有定义成变量去引用的静态值
								},
							},
						}),
					],
				},
			});
		}

		return config;
	}
);
