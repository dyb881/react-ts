window.config = {
	// 移动端调试工具启用
	vconsole: false,
	eruda: false,

	// 请求配置
	request: {
		host: '', // API地址
		apiPath: '', // API入口路径
		photoPath: '', // 图片文件路径
		timeout: null, // 请求超时限制
	},
};
